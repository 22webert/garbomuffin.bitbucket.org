// ==UserScript==
// @name         Restricted Mode Bypass
// @namespace    https://garbomuffin.bitbucket.io/userscripts/restricted-mode-bypass
// @version      2.1.0
// @description  "I like restricted mode!" -Said nobody ever.
// @author       GarboMuffin
// @match        https://www.youtube.com/*
// @downloadURL  https://garbomuffin.bitbucket.io/userscripts/restricted-mode-bypass/restricted-mode-bypass.user.js
// @updateURL    https://garbomuffin.bitbucket.io/userscripts/restricted-mode-bypass/restricted-mode-bypass.meta.js
// ==/UserScript==
