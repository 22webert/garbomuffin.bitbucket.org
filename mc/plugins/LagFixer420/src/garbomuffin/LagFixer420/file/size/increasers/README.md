The purpose of this directory is to increase the file size of the final jar to make this plugin seem more legit than it is. The files have been removed from this because reasons. They had no actual functionality.

This directory contained 26 files (a-z.java) each with the following content:

```java
package garbomuffin.LagFixer420.file.size.increasers;

public class a {
    void a(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void b(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void c(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void d(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void e(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void f(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void g(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void h(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void i(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void j(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void k(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void l(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void m(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void n(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void o(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void p(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void q(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void r(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void s(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void t(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void u(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void v(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void w(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void x(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void y(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
    void z(){a();b();c();d();e();f();g();h();i();j();k();l();m();n();o();p();q();r();s();t();u();v();w();x();y();z();}
}
```

The jar's file size without these: ~`4KB`  
The jar's file size with these: ~`133KB`  
ClerLagg's file size: ~`108KB` (rekt)
